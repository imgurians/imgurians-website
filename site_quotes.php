<?
include_once("bootstrap.inc.php");

$TITLE = "quotes";
include_once("header.inc.php");

$perPage = 20;

echo "<div id='quotes'>";

?>
<form method="get" id='quotes-filter-form'>
  <div id="sort-container">
    Sort by:
    <select name="view" onchange="this.form.submit()" id="sort-dropdown">
      <option<?=_html($_GET["view"]=="newest"?" selected='selected'":"")?> value="newest">newest</option>
      <option<?=_html($_GET["view"]=="oldest"?" selected='selected'":"")?> value="oldest">oldest</option>
      <option<?=_html($_GET["view"]=="best"?" selected='selected'":"")?> value="best">best</option>
      <option<?=_html($_GET["view"]=="worst"?" selected='selected'":"")?> value="worst">worst</option>
      <option<?=_html($_GET["view"]=="random"?" selected='selected'":"")?> value="random">random</option>
    </select>
  </div>
  <a id='submit-quote' title="Submit a quote" href="<?=SITE_URL?>quotes/submit/">Submit</a>
  <div id="search-container">
    <input placeholder="Search&hellip;" name="q" type="text" value="<?=_html($_GET["q"])?>" id="search" />
    <input type="submit" value="OK" />
  </div>
</form>
<?

$sql = new SQLSelect();
$sql->AddTable("quotes");
$sql->AddJoin("left","vote_display_nocache","vote_display_nocache.q_id = quotes.id");
if ($currentUser && $currentUser->eggdropUser)
{
  $sql->AddJoin("left","votes",sprintf_esc("votes.q_id = quotes.id and egg_handle='%s'",$currentUser->eggdropUser));
}
if ($_GET["q"])
{
  $terms = split_search_terms( $_GET["q"] );
  foreach ($terms as $t)
    $sql->AddWhere(sprintf_esc("quote like '%%%s%%'",$t));
}
if ($_GET["quote"])
  $sql->AddWhere(sprintf_esc("id = %d",$_GET["quote"]));

$sqlTotal = clone $sql;
$sqlTotal->AddField("count(*) as c");
$totalCount = SQLLib::SelectRow( $sqlTotal->GetQuery() )->c;

switch($_GET["view"])
{
  case "best":
    $sql->AddOrder("COALESCE((pos*1.0-neg)/(pos+neg+zero+1), 0) DESC");
    break;
  case "worst":
    $sql->AddOrder("COALESCE((pos*1.0-neg)/(pos+neg+zero+1), 0)");
    break;
  case "random":
    $sql->AddOrder("RANDOM()");
    break;
  case "oldest":
    $sql->AddOrder("submit_time");
    break;
  default:
    $sql->AddOrder("submit_time DESC");
    break;
}

$sql->SetLimit($perPage,$_GET["page"] ? (($_GET["page"] - 1) * $perPage) : 0);

$quotes = SQLLib::SelectRows( $sql->GetQuery() );

echo "<div id='quotelist'>";
foreach($quotes as $quote)
{
  printf("<div id='q%d' class='quote'>\n",$quote->id);
  if ($quote->comment)
    echo "<div class='comment'>"._html($quote->comment)."</div>\n";
  echo "<pre class='quotetext'>"._html($quote->quote)."</pre>\n";
  echo "<div class='timestamp'>\n";
  echo "<small>\n";
  printf("<a href='".SITE_URL."quotes/%d'>#%d</a> submitted by %s on %s\n",$quote->id,$quote->id,$quote->nick,date("Y-m-d H:i:s",$quote->submit_time));
  echo "<span data-quoteid='".(int)$quote->id."' class='votes".($quote->value!==null?" voted":"")."'>\n";
  printf("<span data-vote='1'  title='Upvotes' class='upvote".($quote->value===1?" voted":"")."'>%d</span>\n",$quote->pos);
  printf("<span data-vote='0'  title='Meh' class='meh".($quote->value===0?" voted":"")."'>%d</span>\n",$quote->zero);
  printf("<span data-vote='-1' title='Downvotes' class='downvote".($quote->value===-1?" voted":"")."'>%d</span>\n",$quote->neg);
  echo "</span>\n";
  echo "</small></div>\n";
  echo "</div>\n";
}
echo "</div>";

paginator($totalCount,$perPage);

echo "</div>";

if ($currentUser && $currentUser->eggdropUser)
{
?>
<script type="text/javascript">
<!--
document.observe("dom:loaded",function(){
  $$(".quote .votes").each(function(item){
    item.addClassName("clickable");
    var quoteID = item.getAttribute("data-quoteid");
    item.select("span").each(function(arrow){
      var vote = arrow.getAttribute("data-vote");
      arrow.observe("click",function(){
        new Ajax.Request("<?=SITE_URL?>ajax/quotes/",{
          "method":"post",
          "parameters":{
            "vote":vote,
            "quoteID":quoteID,
          },
          onSuccess:function(transport){
            if (transport.responseJSON.success)
            {
              item.addClassName("voted");
              item.select("span").invoke("removeClassName","voted");
              item.down("span[data-vote="+transport.responseJSON.vote+"]").addClassName("voted");
              item.down("span[data-vote=1]").update( transport.responseJSON.voteCount.pos );
              item.down("span[data-vote=0]").update( transport.responseJSON.voteCount.zero );
              item.down("span[data-vote=-1]").update( transport.responseJSON.voteCount.neg );
            }
            if (transport.responseJSON.error)
            {
              alert( transport.responseJSON.message );
            }
          },
        });
      });
    });
  });
});
//-->
</script>
<?
}
include_once("footer.inc.php");
?>