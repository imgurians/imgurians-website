<?
if (!file_exists("config.inc.php"))
  die("config.inc.php not found!");
  
include_once("config.inc.php");
if (defined("SQLITE_FILE"))
  include_once("sqllib-sqlite3.inc.php");
else
  include_once("sqllib-mysql.inc.php");

include_once("functions.inc.php");
include_once("sceneid3lib-php/sceneid3.inc.php");
include_once("imgur-oauth.inc.php");
include_once("im-list.inc.php");

$lifetime = 60 * 60 * 24 * 365;
@ini_set('session.cookie_lifetime', $lifetime);

session_name("IMGURIANSESSION");
session_set_cookie_params($lifetime);

@session_start();

$imgur = new ImgurOauth(array(
  'clientID'     => OAUTH_CLIENT_ID,
  'clientSecret' => OAUTH_CLIENT_SECRET,
  'redirectURI'  => OAUTH_REDIRECT_URI,
));

$currentUser = null;
if (is_logged_in())
  $currentUser = SQLLib::SelectRow(sprintf_esc("select * from users where id=%d",$_SESSION["userID"]));

?>