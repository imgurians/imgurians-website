<?
include_once("rewriter.inc.php");

global $rewriter;
$rewriter = new Rewriter();
$rewriter->addRules(array(
  // browser
  "^\/+people\/?$" => "site_people.php",
  "^\/+profile\/?$" => "site_profile.php",

  "^\/+faq\/?$" => "site_faq.php",
  "^\/+faq\/connecting\/?$" => "site_faq_connecting.php",

  "^\/+news\/?$" => "index.php",
  "^\/+news\/archive\/?$" => "index.php?all=1",
  "^\/+news\/submit\/?$" => "site_add_news.php",

  "^\/+new\-user\/?$" => "site_newuser.php",

  "^\/+gallery\/?$" => "site_gallery.php",

  "^\/+quotes\/([0-9]+)?$" => "site_quotes.php?quote=$1",
  "^\/+quotes\/submit\/?$" => "site_quotes_submit.php",
  "^\/+quotes\/?$" => "site_quotes.php",

  "^\/+secret\-santa\/?$" => "site_secretsanta.php",

  "^\/+events\/?$" => "site_events.php",
  "^\/+topics\/?$" => "site_topics.php",
  
  "^\/+login\/?$" => "login.php",
  "^\/+logout\/?$" => "logout.php",

  "^\/+ajax\/quotes\/?$" => "ajax_quotes.php",

  "^\/+rss\/?$" => "rss.php",
  "^\/+rss\/quotes\/?$" => "rss_quotes.php",
  
  "^\/+chat\/?$" => "chat/index.html",

  "^\/?$" => "index.php",
));
$rewriter->addBootstrap("./bootstrap.inc.php");
$rewriter->addEntryPoint("ENTRY_POINT");
$rewriter->rewrite();
?>