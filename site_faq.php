<?
include_once("bootstrap.inc.php");

$TITLE = "faq";
include_once("header.inc.php");
?>
<h2>Fah-Q</h2>  

<dl id="faq">

<dt>Who the heck are you?</dt>  
<dd>Just a bunch of people who hung around imgur!</dd>

<dt>When did you start the chat?</dt>  
<dd>The <a href='http://imgur.com/gallery/KXwW7'>first post</a> was
  made on December 8th, 2012; the chat has been going non-stop
  pretty much since, and most people stuck around for it too!</dd>

<dt>Did you ever try posting about this on imgur?</dt>  
<dd>Sure, <a href='<?=SITE_URL?>gallery'>take a look</a>!</dd>

<dt>The webchat sucks! Is there a better way to get on the chat?</dt>  
<dd>Sure! There's plenty of ways, head over <a href="<?=SITE_URL?>faq/connecting/">here</a>
  for explanations.</dd>

<dt>What does the @ sign mean in front of people's names?</dt>  
<dd>Those people are the admins of the chat. If you have a problem YO they'll solve it.</dd>

<dt>...and the + sign?</dt>  
<dd>That doesn't mean much, actually. Normally it's to highlight people who are allowed to talk
  when the channel is in read-only mode, but that never happens, it's mostly just decoration.
  Consider it an achievement unlocked if you get one.</dd>

</dl>

<p>
There's also the <a href="<?=SITE_URL?>oldfaq.html">previous FAQ</a> which is no longer 
being updated but contains relevant information for some people. We'll continue to migrate
information from there to here over time.
</p>

<?
include_once("footer.inc.php");
?>
