<?
if ($_GET["error"])
{
  header("Location: ".SITE_URL);
}
else if ($_GET["code"])
{
  $imgur->ProcessAuthResponse();

  unset( $_SESSION["userID"] );
  unset( $_SESSION["imgurID"] );
  
  $result = $imgur->Me();
  
  if ($result)
  {
    $_SESSION["imgurID"] = $result["data"]["id"];

    $user = SQLLib::SelectRow(sprintf_esc("select * from users where imgurID = %d",$_SESSION["imgurID"]));
    if ($user)
    {
      // existing user
      $_SESSION["userID"] = $user->id;
      header("Location: ".SITE_URL.$_SESSION["return"]);
      unset($_SESSION["return"]);
    }
    else
    {
      // new user needs voucher
      header("Location: ".SITE_URL."new-user/");
    }
  }  
  else
  {
    header("Location: ".SITE_URL);
  }
}
else
{
  $_SESSION["return"] = ltrim($_GET["return"],'/');
  unset( $_SESSION["userID"] );
  $imgur->PerformAuthRedirect();
}
?>