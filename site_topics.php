<?
include_once("bootstrap.inc.php");

$TITLE = "channel topics";
include_once("header.inc.php");

$topicsTable = "/home/eggderp/eggderp/data/topics.db";
if (file_exists($topicsTable))
  SQLLib::Query("attach '".$topicsTable."' as _topics");

$perPage = 20;

echo "<ul id='topics'>";

$sql = new SQLSelect();
$sql->AddTable("topics");

$sqlTotal = clone $sql;
$sqlTotal->AddField("count(*) as c");
$totalCount = SQLLib::SelectRow( $sqlTotal->GetQuery() )->c;

$sql->AddOrder("ts DESC");
$sql->SetLimit($perPage,$_GET["page"] ? (($_GET["page"] - 1) * $perPage) : 0);

$topics = SQLLib::SelectRows( $sql->GetQuery() );

function parseIRCcodes($s)
{
  $out = "";
  $bold = false;
  $color = false;
  $colors = array(
    0=> "rgb(255,255,255)",
    1=> "rgb(0,0,0)",
    2=> "rgb(0,0,127)",
    3=> "rgb(0,147,0)",
    4=> "rgb(255,0,0)",
    5=> "rgb(127,0,0)",
    6=> "rgb(156,0,156)",
    7=> "rgb(252,127,0)",
    8=> "rgb(255,255,0)",
    9=> "rgb(0,252,0)",
    10=> "rgb(0,147,147)",
    11=> "rgb(0,255,255)",
    12=> "rgb(0,0,252)",
    13=> "rgb(255,0,255)",
    14=> "rgb(127,127,127)",
    15=> "rgb(210,210,210)",
  );
  for($x=0; $x<strlen($s); $x++)
  {
    switch(ord($s{$x}))
    {
      case 0x02:
        $out .= $bold ? "</b>" : "<b>";
        $bold = !$bold;
        break;
      case 0x03:
        if ($color) $out .= "</span>";
        $fore = "";
        for($x++; $x<strlen($s) && is_numeric($s{$x}); $x++) { $fore.=$s{$x}; }
        $out .= "<span style='color:".$colors[(int)$fore].";";
        if ($s{$x} == ",")
        {
          $back = "";
          for($x++; $x<strlen($s) && is_numeric($s{$x}); $x++) { $back.=$s{$x}; }
          $out .= "background:".$colors[(int)$back].";";
        }        
        $out .= "'>";
        $color = true;
        $x--;
        break;
      case 0x0f:
        if ($bold) { $out .= "</b>"; $bold = false; }
        if ($underline) { $out .= "</u>"; $underline = false; }
        if ($color) { $out .= "</span>"; $color = false; }
        break;
      case 0x1f:
        $out .= $underline ? "</u>" : "<u>";
        $underline = !$underline;
        break;
      default:
        $out .= $s{$x};
        break;
    }
  }
  if ($bold) { $out .= "</b>"; $bold = false; }
  if ($underline) { $out .= "</u>"; $underline = false; }
  if ($color) { $out .= "</span>"; $color = false; }
  return $out;
}

foreach($topics as $topic)
{
  $s = _html($topic->topic);
  $s = parseIRCcodes($s);
  $s = preg_replace("/([a-z]+:\/\/\S+)/","<a href='$1'>$1</a>",$s);
  echo "<li>";
  echo "  <span class='topic'>".$s."</span>";
  echo "  <span class='author'>Set by "._html($topic->nick)." on ".date("Y-m-d H:i:s",$topic->ts)."</span>";
  echo "</li>";
}
echo "</ul>";

paginator($totalCount,$perPage);

include_once("footer.inc.php");
?>