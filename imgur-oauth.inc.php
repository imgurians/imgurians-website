<?
class ImgurOauth extends SceneID3OAuth
{
  const ENDPOINT_TOKEN = "https://api.imgur.com/oauth2/token";
  const ENDPOINT_AUTH = "https://api.imgur.com/oauth2/authorize";
  const ENDPOINT_TOKENINFO = "";
  const ENDPOINT_RESOURCE = "https://api.imgur.com/3/";

  function Me()
  {
    $data = $this->ResourceRequestRefresh( static::ENDPOINT_RESOURCE . "account/me/" );
    return $this->UnpackFormat( $data );
  }
  function GetAlbum($id)
  {
    $data = $this->ResourceRequestRefresh( static::ENDPOINT_RESOURCE . "album/".$id );
    return $this->UnpackFormat( $data );
  }
}

?>