<?
include_once("bootstrap.inc.php");

$TABLE = "secretsanta_2015";

function decode_address($text){ return base64_decode(str_rot13($text)); }
function encode_address($text){ return str_rot13(base64_encode($text)); }

forceLoggedIn();

$drawDone = !!SQLLib::SelectRow(sprintf_esc("select * from ".$TABLE." where targetID is not null"));

$application = SQLLib::SelectRow(sprintf_esc("select * from ".$TABLE." where userID=%d",$currentUser->id));

$error = "";
if ($_POST["address"])
{
  if ($application)
  {
    SQLLib::UpdateRow($TABLE,array("address"=>encode_address($_POST["address"])),sprintf_esc("userID=%d",$currentUser->id));
  }
  else
  {
    SQLLib::InsertRow($TABLE,array(
      "address"=>encode_address($_POST["address"]),
      "userID"=>$currentUser->id,
      "applicationDate"=>date("Y-m-d H:i:s"),
    ));
  }
  header("Location: ".SITE_URL."secret-santa/");
}
if ($_POST["confirm"])
{
  SQLLib::UpdateRow($TABLE,array("confirmationDate"=>date("Y-m-d H:i:s")),sprintf_esc("userID=%d",$currentUser->id));
  header("Location: ".SITE_URL."secret-santa/");
}
if ($_POST["shipping"])
{
  SQLLib::UpdateRow($TABLE,array("shippingDate"=>date("Y-m-d H:i:s")),sprintf_esc("userID=%d",$currentUser->id));
  header("Location: ".SITE_URL."secret-santa/");
}
if (is_uploaded_file($_FILES["picture"]["tmp_name"]))
{
  if (filesize($_FILES["picture"]["tmp_name"]) > 1024 * 1024)
  {
    $error = "Please keep the filesize under 1MB okay?";
  }
  else
  {
    SQLLib::UpdateRow($TABLE,array(
      "picture"=>base64_encode(file_get_contents($_FILES["picture"]["tmp_name"])),
      "pictureComment"=>$_POST["pictureComment"],
    ),sprintf_esc("userID=%d",$currentUser->id));
    header("Location: ".SITE_URL."secret-santa/");
  }
}

$TITLE = "secret santa!";
include_once("header.inc.php");

$stats = SQLLib::SelectRow(sprintf_esc("select count(*) as total, sum(case when shippingdate is null then 0 else 1 end) as shipped, sum(case when picture is null then 0 else 1 end) as pictures from ".$TABLE));
$statsText = sprintf("<p class='secretsanta-stats'>A total of <span>%d</span> people signed up; <span>%d</span> gifts have been now shipped, with <span>%d</span> people having already received them!</p>\n",$stats->total,$stats->shipped,$stats->pictures);

if ($application && $application->targetID)
{
  // we have a pair

  $mySanta = SQLLib::SelectRow(sprintf_esc("select * from ".$TABLE." where targetID=%d",$currentUser->id));
  if ($mySanta->shippingDate)
    echo "<p class='success'>Your secret Santa has sent your gift in the mail! Squee!</p>";
  else if ($mySanta->confirmationDate)
    echo "<p class='success'>Your secret Santa confirmed your address and is now on the case! Oh the excitement!</p>";

  $recipient = SQLLib::SelectRow(sprintf_esc("select ".$TABLE.".*, users.name from ".$TABLE." left join users on users.id = ".$TABLE.".userID where userID=%d",$application->targetID));

  echo "<h2>The Secret Santa draw is done</h2>";
  echo "<h3>You're sending to <span>"._html($recipient->name)."</span>!</h3>";
  
  echo "<p>Their name and address you should be sending your gift to is this:</p>";
  
  echo "<pre class='quotelike'>"._html(decode_address($recipient->address))."</pre>\n";

  if (!$application->confirmationDate)
  {
    echo "<form method='post'>";
    echo "  <p>Press this button to acknowledge the giftee. This lets them know that you're on the case!</p>";
    echo "  <input type='submit' name='confirm' value=\"Yup, I know who to send the gift to!\">";
    echo "</form>";
  }
  else if (!$application->shippingDate)
  {
    echo "<form method='post'>";
    echo "  <p>Press this button to tell the giftee that the gift is en route and they should be expecting it!</p>";
    echo "  <input type='submit' name='shipping' value=\"Yup, it's all in the mail now!\">";
    echo "</form>";
  }
  else
  {
    echo "  <p class='success'>You're done, congrambulations!</p>";
  }
  echo "<div id='secret-santa-infographics'>";
  echo "<h2>The #imgurians Secret Santa infographics!</h2>";
  if ($error) echo "<div class='error'>"._html($error)."</div>";
  echo "<form method='post' enctype='multipart/form-data'>";
  echo "  <img style='float:right;margin-left:10px;' src='".SITE_URL."imgur_santa.png'/>";
  echo $statsText;
  echo "  <p>So we had this idea to collect photos of the gifts they <b>received</b> (or of people with their gifts) and make a nice big post about it on Imgur. It'd look something like what you see on the right:</p>";
  echo "  <p>If you want to help with that, you can upload a picture of what you got here: <small>(please be reasonable and stay under 1MB or so)</small></p>";
  echo "  <input type='file' name='picture' accept='image/*'>";
  echo "  <p>It also helps if you describe what it is or have a message you'd like to add next to the picture:</p>";
  echo "  <textarea name='pictureComment'>"._html($application->pictureComment)."</textarea>";
  echo "  <input type='submit' name='pictureUpload' value=\"Upload!\">";
  echo "</form>";
  echo "</div>";
  
}
else
{

  if ($drawDone)
  {
    echo "<h2>Too late!</h2>";
    echo "<p>The Secret Santa is closed and the draw has been done - sorry if you missed out :(</p>";
    echo $statsText;
  }
  else
  {
    echo "<h2>Join the Secret Santa!</h2>";
  
    echo "<p>Yay, it's that time of the year again, come join us for the channel's random-gift-giveaway! Simply enter your address below ".
         "to sign up for the draw, and then come back after the draw to see who you should be sending your gift to!</p>";
    echo "<p>Here's a couple of things you should be keeping in mind:</p>";
    echo "<ul>\n";
    echo "<li>The draw is going to be at <b>December 5th, 2015</b>, so make sure you submit your address before then.</li>\n";
    echo "<li>If you don't feel comfortable sharing your address, you can consider doing it \n";
    echo "  <a href='https://en.wikipedia.org/wiki/Poste_restante'>poste restante</a> - find out if it's possible in your country!</li>\n";
    echo "<li>Be reasonable, <b>aim for something that's at most \$50 (shipping excluded)</b>. \n";
    echo "  This is by no means a strict regulation (and of course there's always price disparity), but we don't want anyone to feel left out.</li>\n";
    echo "<li>Don't forget to add <b>your name</b> on top of the address, and <b>the country</b> \n";
    echo "  at the bottom. The rest should be formatted according to the custom of your country.</li>\n";
    echo "<li>We're all over the world, so <b>it's possible that your gift will require expensive shipping</b>. Keep that in mind.</li>\n";
    echo "</ul>\n";
  
    if ($application)
    {
      echo "<p class='success'><b>You've signed up for the Secret Santa!</b> You can still update/change your shipping address though.</p>";
    }
    echo "<form method='post'>";
    echo "  <label for='address'>Your name and address: <span class='required'>Required</span></label>";
    echo "  <textarea name='address' id='address' required='yes'>"._html(decode_address($application->address))."</textarea>";
    echo "  <input type='submit' value='".($application?"Update":"Sign up!")."'>";
    echo "</form>";
  }
}

include_once("footer.inc.php");
?>