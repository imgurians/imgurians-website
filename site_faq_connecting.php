<?
include_once("bootstrap.inc.php");
include_once("header.inc.php");
?>
<h2>quik how i do irc</h2>  

<h3>If you already know how to use an IRC client...</h3>

<p>
We're on IRCNet (<a href="http://irc.tu-ilmenau.de/all_servers/">here's a server list</a>) on the <b>#imgurians</b> channel!
That's all you really need to know!
</p>

<h3>If you have no idea how to use an IRC client, and don't really want to bother either...</h3>

<p>
The fastest way is probably to use <a href="http://irccloud.com/">IRCCloud</a>; it works in
browser and on your phone too without too much hassle.
<div class='foldable'>
  <span class='handle'>Show me how to set up IRCCloud &raquo;</span>
  <div class='contents'>
    hello.jpg
  </div>
</div>

</p>

<h3>If you have no idea how to use an IRC client, but don't mind tinkering...</h3>

<p>
There are many options you can choose from:
</p>

<ul>
  <li>On Windows, probably the most popular client is <a href="http://www.mirc.com/">mIRC</a> -
    it's not free, but it's quite easy to use.
    <div class='foldable'>
      <span class='handle'>Show me how to set up mIRC &raquo;</span>
      <div class='contents'>
        hello.jpg
      </div>
    </div>
  </li>
  <li>Another option on Windows / OSX / Linux is , probably the most popular client is <a href="https://hexchat.github.io/">Hexchat</a> -
    not the prettiest client, but it works pretty well.
    <div class='foldable'>
      <span class='handle'>Show me how to set up Hexchat &raquo;</span>
      <div class='contents'>
        hello.jpg
      </div>
    </div>
  </li>
</ul>

<script type="text/javascript">
<!--
document.observe("dom:loaded",function(){
  $$(".foldable").each(function(item){
    item.down(".contents").hide();
    item.down(".handle").observe("click",function(){
      item.down(".contents").toggle();
    });
  });
});
//-->
</script>
<?
include_once("footer.inc.php");
?>