<?
/*
 * The following code bits are for a proof-of-work algorithm.
 *
 * The server sends a random string/number to the client,
 * the client then performs a number of hashing operations on
 * that string and inserts the result in the form via JS.
 *
 * If the client doesn't support JS, a simple human-readable
 * CAPTCHA is shown.
 *
 * If the result is present in the POST request values,
 * the server performs the same hashing and matches the result.
 *
 * If the result is not present, the server falls back to the
 * human-readable test.
 */

class Captcha
{
  const PROOFOFWORK_ITERATIONS = 1000;

  function __construct($context = "")
  {
    $this->context = "";
  }
  function hashCode( $s )
  {
    $hash = 0;
    for ($i = 0; $i < strlen($s); $i++)
    {
      $chr = ord( $s{$i} );
      $hash = (($hash << 5) - $hash) + $chr;
      $hash &= 0x7FFFFFFF;
    }
    return $hash;
  }
  function calcHash($s)
  {
    for ($i = 0; $i < self::PROOFOFWORK_ITERATIONS; $i++)
    {
      $s = $this->hashCode( "hash" . $s );
    }
    return $s;
  }
  function TestValues( &$error )
  {
    if ($_POST["country"])
    {
      $error = "You failed the human test miserably!";
      return false;
    }
    if ($_POST["jsTest"])
    {
      if ($_POST["jsTest"] != $this->calcHash( $_SESSION[$this->context . "captchaOrigHash"] ))
      {
        $error = "Your computer failed the computer test!";
        return false;
      }
    }
    else
    {
      if ($_POST["test"] != $_SESSION[$this->context . "captchaNoJSsolution"])
      {
        $error = "You failed the human test!";
        return false;
      }
    }
    return true;
  }

  function PrintFormComponents()
  {
    $numbers    = array( "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten" );
    $notNumbers = array( "duck", "table", "fish", "nose", "cube", "button", "pizza", "stick", "cow", "book" );

    if (rand(0,99) < 50)
    {
      $correct = $numbers;
      $incorrect = $notNumbers;
      $label = "Which one of these is a number?";
    }
    else
    {
      $incorrect = $numbers;
      $correct = $notNumbers;
      $label = "Which one of these is NOT a number?";
    }
    $a = array();
    $a[0] = $correct[ array_rand($correct) ];
    $_SESSION[$this->context . "captchaNoJSsolution"] = $a[0];
    do {
      $a[] = $incorrect[ array_rand($incorrect) ];
      $a = array_unique($a);
    } while(count($a) < 5);

    shuffle($a);
    $label .= " <span class='testValues'>" . implode(", ",$a) . "</span>";

    $_SESSION[$this->context . "captchaOrigHash"] = rand(1,9999);
?>
  <div id='nojs'>
    <label><?=$label?></label>
    <input type='text' name='test'/>
  </div>
  <div style='display:none;'>
    <label>Don't fill this field!</label>
    <input type='text' name='country'/>
  </div>
  <script type="text/javascript">
  <!--
  document.observe("dom:loaded",function(){
    $("nojs").hide();

    function hashCode( s )
    {
      var hash = 0;
      for (var i = 0; i < s.length; i++)
      {
        var chr  = s.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash &= 0x7FFFFFFF;
      }
      return hash;
    };
    var x = <?=$_SESSION[$this->context . "captchaOrigHash"]?>;
    for (var i = 0; i < <?=self::PROOFOFWORK_ITERATIONS?>; i++)
    {
      x = hashCode( "hash" + x );
    }
    $("nojs").insert( new Element("input",{type:"hidden","name":"jsTest","value":x}) );
  });
  //-->
  </script>
<?
  }
}
?>