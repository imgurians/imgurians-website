<?
include_once("bootstrap.inc.php");

if (!is_logged_in())
{
  header("Location: ".SITE_URL);
  exit();
}

if ($_POST["title"] && $_POST["contents"])
{
  $a = array();
  $a["userID"] = $currentUser->id;
  $a["date"] = date("Y-m-d");
  $a["title"] = $_POST["title"];
  $a["contents"] = $_POST["contents"];
  SQLLib::InsertRow("news",$a);

  header("Location: ".SITE_URL);
  exit();
}

include_once("header.inc.php");

echo "<h2>Say something!</h2>";

echo "<form method='post'>";
echo "  <label for='name'>The news item title</label>";
echo "  <input type='text' name='title' id='title' required='yes'/>";
echo "  <label for='name'>The news item contents</label>";
echo "  <textarea name='contents' id='contents' required='yes'/></textarea>";

echo "  <input type='submit' value='Send!'>";
echo "</form>";

include_once("footer.inc.php");
?>