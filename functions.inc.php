<?
///////////////////////////////////////////////////////////////////////////////

function _html( $s )
{
  return htmlspecialchars($s,ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5,"utf-8");
}
function _js( $s )
{
  return addcslashes( $s, "\x00..\x1f'\"\\/" );
}
function _like( $s )
{
  return addcslashes($s,"%_");
}

function shortify( $text, $length = 100 )
{
  if (mb_strlen($text,"utf-8") <= $length) return $text;
  $z = mb_stripos($text," ",$length,"utf-8");
  return mb_substr($text,0,$z?$z:$length,"utf-8")."...";
}
function split_search_terms( $str )
{
  preg_match_all('/([^\s"]+)|"([^"]*)"/',$str,$m);
  $terms = array();
  foreach($m[0] as $k=>$v)
    $terms[] = $m[1][$k] ? $m[1][$k] : $m[2][$k];
  return $terms;
}

function is_logged_in()
{
  return @!!$_SESSION["userID"];
}
function generate_voucher()
{
  return strtoupper(substr(sha1("K7bUAJ3W7p6g6Dz".time().rand(1000,9999)),0,8));
}

function paginator($totalCount,$perPage)
{
  if ($totalCount >= $perPage)
  {
    $pageCount = ceil( $totalCount / $perPage );
    echo "<div id='paginator'>\n";
    echo "<ul>\n";
    if ($_GET["page"] > 1)
    {
      $get = $_GET;
      $get["page"] = $_GET["page"] - 1;
      printf("<li><a href='?%s'>&laquo; Prev</a></li>\n",http_build_query($get));
    }
    for($x = 0; $x < $pageCount; $x++)
    {
      $get = $_GET;
      $get["page"] = $x + 1;
      
      $d = ($x + 1) - ($_GET["page"] ? $_GET["page"] : 1);
      if ($d == 0)
      {
        printf("<li>%d</li>\n",$x + 1);
        $lastWasDigit = true;
      }
      else
      {
        if (($x < 3) || ($pageCount - $x <= 3) || (abs($d) <= 2))
        {
          $lastWasDigit = true;
          printf("<li><a href='?%s'>%d</a></li>\n",http_build_query($get),$x + 1);
        }
        else
        {
          if ($lastWasDigit)
          {
            printf("<li>&hellip;</li>\n");
            $lastWasDigit = false;
          }
        }
      }
    }
    if ($_GET["page"] < $pageCount)
    {
      $get = $_GET;
      $get["page"] = ($_GET["page"] ? $_GET["page"] : 1) + 1;
      printf("<li><a href='?%s'>Next &raquo;</a></li>\n",http_build_query($get));
    }  
    echo "</ul>\n";
    echo "</div>\n";
  }
}

function forceLoggedIn()
{
  if (!is_logged_in())
  {
    global $rewriter;
    include_once("header.inc.php");
    echo "You have to be <a href='"._html(SITE_URL."login/?return=".rawurlencode($rewriter->rootRelativeURL))."'>logged in</a> to view this page!";
    include_once("footer.inc.php");
    exit();
  }
}

function forceReload()
{
  header("Location: ".$_SERVER["REQUEST_URI"]);
  exit();
}

function parseDescription($t)
{
  $s = _html($t);
  $s = str_replace("\r\n","\n",$s);
  $s = preg_replace("/([a-z]+:\/\/\S+)/","<a href='$1'>$1</a>",$s);
  $s = preg_replace("/\n{2,}/","</p>\n<p>",$s);
  return $s;
}

function dateDiffReadable( $t )
{
  $a = time();
  $b = is_string($t) ? strtotime($t) : $t;
  $dif = $a - $b;
  if ($dif > 60 * 60 * 24)
    return substr($t,0,10);
  $s = ($dif % 60) . " seconds" ; $dif = (int)($dif / 60); if (!$dif) return $s . " ago";
  $s = ($dif % 60) . " minutes"; $dif = (int)($dif / 60); if (!$dif) return $s . " ago";
  $s = ($dif % 24) . " hours"; $dif = (int)($dif / 24); return $s . " ago";
}
?>