<?
if(php_sapi_name() != "cli")
  die("commandline only!");

include_once("bootstrap.inc.php");

$TABLE = "secretsanta_2015";

$commit = false;
$mail = false;
for ($x = 1; $x < count($argv); $x++)
{
  switch ($argv[$x])
  {
    case "--commit":
    case "-c":
    {
      $commit = true;
    } break;
    case "--mail":
    case "-m":
    {
      $mail = true;
    } break;
    default:
    {
    } break;
  }
}


$sql = new SQLSelect();
$sql->AddField($TABLE.".userID as id");
$sql->AddField("users.name");
$sql->AddField("users.email");
$sql->AddTable($TABLE);
$sql->AddJoin("left","users","users.id = ".$TABLE.".userid");
$sql->AddOrder("(SELECT CASE WHEN users.countryCode IN ('us','ca','vn') THEN 0 WHEN users.countryCode IN ('sg') THEN 1 ELSE 2 END)");//continent
$sql->AddOrder("RANDOM()");

$applicants = SQLLib::SelectRows( $sql->GetQuery() );

printf("Retrieved %d applicants\n",count($applicants));

if ($mail)
{
  foreach($applicants as $a)
  {
    if(!$a->email) continue;
    printf(" %s\n",$a->name);
    mail($a->email,"#imgurians Secret Santa 2015 - the draw is done!",
      "Hi!\n"
      ."\n"
      ."The draw for the #imgurian Secret Santa is now done - check ".SITE_URL."secret-santa/ on who you got!\n"
      ."\n"
      ."- #imgurians management and board of directors\n",
      "From: no-reply@obs.wzff.de");
  }
}
else
{
  echo "Results: \n";
  for($x=0; $x<count($applicants); $x++)
  {
    printf("  %d. %-15s",$x+1,$applicants[$x]->name);
    if ($commit)
    {
      SQLLib::UpdateRow($TABLE,array("targetID"=>$applicants[($x+1)%count($applicants)]->id),sprintf_esc("userID=%d",$applicants[$x]->id));
      echo "[saved]";
    }
    echo "\n";
  }
  echo "Done.\n";
}

?>