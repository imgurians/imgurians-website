<?
include_once("bootstrap.inc.php");

$TITLE = "the populace";
include_once("header.inc.php");

$countries = json_decode(file_get_contents("countries.json"),true);

$people = SQLLib::SelectRows("select * from users order by name".(defined("SQLITE_FILE")?" COLLATE NOCASE":""));
echo "<h2>Our merry band of giraffes</h2>";
echo "<table id='people'>";

foreach($ims as $imName=>$im)
{
  $has = false;
  foreach($people as $v)
    if ($v->$imName)
      $has = true;
  if (!$has)
    unset($ims[$imName]);
}

foreach($people as $v)
{
  echo "<tr>";
  echo "<td class='name'>"._html($v->name)."</td>";
  echo "<td class='country'>".($v->countryCode ? "<img src='".FLAGS_DIR.$v->countryCode.".png' alt='"._html($countries[$v->countryCode]["english"])."' title='"._html($countries[$v->countryCode]["english"])."'/>" : "&nbsp;")."</td>";
  
  foreach($ims as $imName=>$im)
  {
    if (!is_logged_in() && !$im["public"])
      continue;
    if ($v->$imName)
    { 
      if ($im["transformToLink"])
        printf("<td><a href='%s' class='icon %s'>%s</a></td>\n",$im["transformToLink"]($v->$imName),$imName,$im["name"]);
      if ($im["transformToText"])
        printf("<td>%s</td>\n",$im["transformToText"]($v->$imName));
    }
    else
    {
      printf("<td>&nbsp;</td>");
    }
  }
  echo "</tr>";
}
echo "</table>";

echo "<h2>Other congregational points</h2>";
echo "<ul>";
echo "<li><a href='http://obs.wzff.de/~tard/pisg/'>Channel stats</a></li>";
echo "<li><a href='https://bitbucket.org/imgurians'>Bitbucket team</a> (also includes the source of this site)</li>";
echo "</ul>";

include_once("footer.inc.php");
?>