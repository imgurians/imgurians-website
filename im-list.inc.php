<?
$ims = array(
  "email"=>array(
    "name" => "E-mail",
    "formType" => "email",
    "transformToLink" => function($input)
      {
        return "mailto:".$input;
      },
  ),
  "website"=>array(
    "name" => "Website",
    "formType" => "url",
    "transformToLink" => function($input)
      {
        return $input;
      },
  ),
  "imgur"=>array(
    "public" => true,
    "name" => "imgur",
    "transformToLink" => function($input)
      {
        return "http://imgur.com/user/".$input;
      },
  ),
  "facebook"=>array(
    "name" => "Facebook",
    "transformInput" => function($input)
      {
        if (preg_match("/facebook\.com\/([a-z0-9A-Z\_\.]+)/",$input,$matches))
          return $matches[1];
        return $input;
      },
    "transformToLink" => function($input)
      {
        return "https://facebook.com/".$input;
      },
  ),
  "twitter"=>array(
    "name" => "Twitter",
    "transformInput" => function($input)
      {
        if (preg_match("/twitter\.com\/#?\/?([a-z0-9A-Z\_]+)/",$input,$matches))
          return $matches[1];
        if (preg_match("/@([a-z0-9A-Z\_]+)/",$input,$matches))
          return $matches[1];
        return $input;
      },
    "transformToLink" => function($input)
      {
        return "https://twitter.com/".$input;
      },
  ),
  "instagram"=>array(
    "name" => "Instagram",
    "transformInput" => function($input)
      {
        if (preg_match("/instagram\.com\/([a-z0-9A-Z\_]+)/",$input,$matches))
          return $matches[1];
        return $input;
      },
    "transformToLink" => function($input)
      {
        return "https://instagram.com/".$input;
      },
  ),
  "snapchat"=>array(
    "name" => "Snapchat",
    "transformToLink" => function($input)
      {
        return "https://www.snapchat.com/add/".$input;
      },
  ),
  "skype"=>array(
    "name" => "Skype",
    "transformToLink" => function($input)
      {
        return "skype:"._html($input)."?chat";
      },
  ),
  "googleplus"=>array(
    "name" => "Google+",
    "transformInput" => function($input)
      {
        if (preg_match("/plus\.google\.com\/([a-z0-9A-Z\_\+]+)/",$input,$matches))
          return $matches[1];
        return $input;
      },
    "transformToLink" => function($input)
      {
        return "https://plus.google.com/"._html($input);
      },
  ),
  "steam"=>array(
    "name" => "Steam",
    "note" => "(your Steam profile URL)",
    "transformInput" => function($input)
      {
        if (preg_match("/steamcommunity\.com\/id\/([a-z0-9A-Z\_]+)/",$input,$matches))
          return $matches[1];
        if (preg_match("/steamcommunity\.com\/profiles\/([0-9]+)/",$input,$matches))
          return $matches[1];
        return $input;
      },
    "transformToLink" => function($input)
      {
        if (is_numeric($input))
          return "http://steamcommunity.com/profiles/"._html($input)."/";
        return "http://steamcommunity.com/id/"._html($input)."/";
      },
  ),
  "websiteTagline"=>array(
    "name" => "Tagline",
    "public" => true,
    "note" => "(just write something witty here)",
    "maxlength" => 32,
    "transformInput" => function($input)
      {
        return substr($input,0,32);
      },
    "transformToText" => function($input)
      {
        return "<span class='tagline'>"._html($input)."</span>";
      },
  ),
);
?>