<?
include_once("bootstrap.inc.php");

if (!$_SESSION["imgurID"])
{
  header("Location: ".SITE_URL);
  exit();
}

$error = "";
if ($_POST["voucher"])
{
  $row = SQLLib::SelectRow(sprintf_esc("select * from users where voucherCode = '%s'",trim($_POST["voucher"]," \"'")));
  if ($row || (defined("VOUCHER_SKELETONKEY") && $_POST["voucher"] == VOUCHER_SKELETONKEY))
  {
    $data = $imgur->Me();
    
    $a = array();
    $a["imgurID"] = $data["data"]["id"];
    $a["imgur"] = $data["data"]["url"];
    $a["name"] = $data["data"]["url"];
    $a["voucherCode"] = generate_voucher();
    if ($row)
      $a["voucherUserID"] = $row->id;
    $id = SQLLib::InsertRow("users",$a);
  
    $_SESSION["userID"] = $id;
    
    // generate new code
    if ($row)
      SQLLib::UpdateRow("users",array("voucherCode"=>generate_voucher() ),"id=".(int)$row->id);
    
    header("Location: ".SITE_URL."profile/");
    exit();
  }
  else
  {
    $error = "Can't find that voucher code! Make sure you copied it correctly!";
  }
}

include_once("header.inc.php");
?>
  <h2>Welcome to #imgurians</h2>  
  
  <?
  if ($error)
    printf("<h3 class='error'>%s</h3>",$error);
  ?>
  
  <p>New to the community? You need someone to vouch for you - ask around on the chat!</p>
  
  <form method='post'>
    <label for='voucher'>Enter the voucher code you got here:</label>
    <input name='voucher' id='voucher' type='text' required='yes'/>
    <input type='submit' value='Send!'/>
  </form>
<?
include_once("footer.inc.php");
?>