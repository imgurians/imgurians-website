<?
include_once("bootstrap.inc.php");

forceLoggedIn();

if ($_POST["locationVote"])
{
  SQLLib::Query(sprintf_esc("delete from events_locations_votes where eventID = %d and userID = %d",$_GET["id"],$currentUser->id));
  foreach($_POST["location"] as $k=>$v)
    SQLLib::InsertRow("events_locations_votes",array("eventID"=>$_GET["id"],"locationID"=>$k,"userID"=>$currentUser->id));
  forceReload();
}
if ($_POST["timeVote"])
{
  SQLLib::Query(sprintf_esc("delete from events_times_votes where eventID = %d and userID = %d",$_GET["id"],$currentUser->id));
  foreach($_POST["time"] as $k=>$v)
    SQLLib::InsertRow("events_times_votes",array("eventID"=>$_GET["id"],"timeID"=>$k,"userID"=>$currentUser->id));
  forceReload();
}
if ($_POST["comment"])
{
  SQLLib::InsertRow("events_comments",array(
    "eventID"=>$_GET["id"],
    "comment"=>$_POST["comment"],
    "userID"=>$currentUser->id,
    "commentDate"=>date("Y-m-d H:i:s"),
  ));
  forceReload();
}

$TITLE = "events";
include_once("header.inc.php");

$sql = new SQLSelect();
$sql->AddTable("events");
$sql->AddField("events.*");
$sql->AddField("events_locations.*");
$sql->AddField("events_times.*");
$sql->AddField("events.id as id");
$sql->AddField("users.name as username");
$sql->AddJoin("left","users","users.id = events.posterid");
$sql->AddJoin("left","events_locations","events_locations.id = events.locationID");
$sql->AddJoin("left","events_times","events_times.id = events.timeID");
$sql->AddOrder("postDate desc");
$sql->AddOrder("id desc");

if ($_GET["id"])
{
  echo "<div id='event'>";
  $sql->AddWhere(sprintf_esc("events.id=%d",$_GET["id"]));
  $event = SQLLib::SelectRow( $sql->GetQuery() );
  if (!$event) exit();
?>
  <h2><?=_html($event->name)?></h2>
  <div class='submitter'>by <?=_html($event->username)?></div>
  <div class='description'>
    <p><?=parseDescription($event->description); ?></p>
  </div>
  <?
    $locationsBeingVoted = false;
    if ($event->location)
    {
      printf("<div class='location'><b>Location:</b> %s</div>\n",$event->location);
    }
    else
    {
      $results = array();
      if ($event->posterID == $currentUser->id)
      {
        $r = SQLLib::SelectRows(sprintf_esc("select events_locations_votes.locationID, count(*) as c from events_locations_votes left join events_locations on events_locations.id = events_locations_votes.locationID where events_locations_votes.eventID = %d group by locationID",$_GET["id"]));
        foreach($r as $v) $results[$v->locationID] = $v->c;
      }
      
      $locations = SQLLib::SelectRows(sprintf_esc("select * from events_locations where eventID = %d",$_GET["id"]));
      $_myVotes = SQLLib::SelectRows(sprintf_esc("select * from events_locations_votes where eventID = %d and userID = %d",$_GET["id"],$currentUser->id));
      $myVotes = array();
      foreach($_myVotes as $v) $myVotes[$v->locationID] = true;
      echo "<h3>Locations</h3>\n";
      echo "<p><b>Note:</b> before the timeslots are being voted on, the locations must be narrowed down; vote for the locations that you can consider attending <u>at all</u>, even if it's not 100%.</p>\n";
      echo "<form method='post'>\n";
      echo "<ul class='votelist'>\n";
      foreach($locations as $l)
      {
        printf("  <li><label>");
        printf("<input type='checkbox' name='location[%d]'%s/> ",_html($l->id),$myVotes[$l->id]?" checked='checked'":"");
        echo _html($l->location);
        if ($results[$l->id])
          printf(" <span class='results'>%d</span>",$results[$l->id]);
        printf("</label></li>\n");
      }
      echo "</ul>\n";
      echo "  <small>You can change your votes at any point until the vote is closed.</small>\n";
      echo "  <input type='submit' name='locationVote' value='Vote!'>\n";
      echo "</form>\n";

      $locationsBeingVoted = true;
    }
    
    if (!$locationsBeingVoted)
    {
      if ($event->timeStart)
      {
        printf("<div class='time'><b>Time:</b> %s - %s</div>\n",$event->timeStart,$event->timeEnd);
      }
      else
      {
        $results = array();
        if ($event->posterID == $currentUser->id)
        {
          $r = SQLLib::SelectRows(sprintf_esc("select events_times_votes.timeID, count(*) as c from events_times_votes left join events_times on events_times.id = events_times_votes.timeID where events_times_votes.eventID = %d group by timeID",$_GET["id"]));
          foreach($r as $v) $results[$v->timeID] = $v->c;
        }

        $times = SQLLib::SelectRows(sprintf_esc("select * from events_times where eventID = %d",$_GET["id"]));
        $_myVotes = SQLLib::SelectRows(sprintf_esc("select * from events_times_votes where eventID = %d and userID = %d",$_GET["id"],$currentUser->id));
        $myVotes = array();
        foreach($_myVotes as $v) $myVotes[$v->timeID] = true;
        echo "<h3>Times</h3>\n";
        echo "<form method='post'>\n";
        echo "<ul class='votelist'>\n";
        foreach($times as $l)
        {
          printf("  <li><label>");
          printf("<input type='checkbox' name='time[%d]'%s/> %s",_html($l->id),$myVotes[$l->id]?" checked='checked'":"",$l->timeEnd?(_html($l->timeStart)." - "._html($l->timeEnd)):_html($l->timeStart));
          if ($results[$l->id])
            printf(" <span class='results'>%d</span>",$results[$l->id]);
          printf("</label></li>\n");
        }
        echo "</ul>\n";
        echo "  <small>You can change your votes at any point until the vote is closed.</small>\n";
        echo "  <input type='submit' name='timeVote' value='Vote!'>\n";
        echo "</form>\n";
      }
    }
  ?>
  <h3>Comments</h3>
<?

  $sql = new SQLSelect();
  $sql->AddTable("events_comments");
  $sql->AddField("events_comments.*");
  $sql->AddField("users.name as username");
  $sql->AddJoin("left","users","users.id = events_comments.userID");
  $sql->AddWhere(sprintf_esc("events_comments.eventID=%d",$_GET["id"]));
  $comments = SQLLib::SelectRows( $sql->GetQuery() );
  printf("<ul class='comments'>");
  foreach($comments as $c)
  {
    printf("<li>");
    printf("  <div class='comment'><p>%s</p></div>",parseDescription($c->comment));
    printf("  <div class='author'>%s <time title='%s'>%s</time></div>",_html($c->username),$c->commentDate,dateDiffReadable($c->commentDate));
    printf("</li>");
  }
  printf("</ul>");
?>
<h4>Add a comment</h4>
<form method="post">
  <textarea id="comment" name="comment"></textarea>
  <input id="submit" type="submit" value="Post">
</form>
<?
  echo "</div>";
}
else
{
echo "<h2>Happenings!</h2>";
echo "  <div id='events'>";
$events = SQLLib::SelectRows( $sql->GetQuery() );
foreach($events as $event)
{
?>
    <article id='events<?=$event->id?>'>
      <h3><a href='<?=SITE_URL?>events/?id=<?=$event->id?>'><?=_html($event->name)?></a></h3>
      <div class='submitter'>by <?=_html($event->username)?></div>
      <?
        if ($event->location)
          printf("<div class='location'><b>Location:</b> %s</div>\n",$event->location);
        else
          printf("<div class='location'><i>Location still under voting!</i></div>\n");
          
        if ($event->timeStart)
          printf("<div class='time'><b>Time:</b> %s - %s</div>\n",$event->timeStart,$event->timeEnd);
        else
          printf("<div class='time'><i>Times still under voting!</i></div>\n");
      ?>
      <div class='description'>
        <p><?=parseDescription($event->description); ?></p>
      </div>
    </article>
<?
}
echo "  </div>";
}
include_once("footer.inc.php");
?>