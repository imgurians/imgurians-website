<?
include_once("bootstrap.inc.php");

$output = array();
if ($currentUser)
{
  if ($currentUser->eggdropUser)
  {
    SQLLib::Query(sprintf_esc("delete from votes where q_id = %d and egg_handle='%s'",$_POST["quoteID"],$currentUser->eggdropUser));
    SQLLib::InsertRow("votes",array("q_id"=>(int)$_POST["quoteID"],"egg_handle"=>$currentUser->eggdropUser,"value"=>(int)$_POST["vote"]));
    $output["success"] = true;
    $output["vote"] = (int)$_POST["vote"];
    $output["voteCount"] = SQLLib::SelectRow(sprintf_esc("select * from vote_display_nocache where q_id = %d",$_POST["quoteID"]));
  }
  else
  {
    $output["error"] = true;
    $output["message"] = "Your account to tard is not connected!";
  }
}
else
{
  $output["error"] = true;
  $output["message"] = "You're not logged in.";
}

header("Content-type: application/json; charset=utf-8");
die( json_encode( $output ) );
?>