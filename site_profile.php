<?
include_once("bootstrap.inc.php");
include_once("rpc.inc.php");

if (!is_logged_in())
{
  header("Location: ".SITE_URL);
  exit();
}
$countries = json_decode(file_get_contents("countries.json"),true);
uksort($countries,function($a,$b)use($countries){ return strcmp($countries[$a]["english"],$countries[$b]["english"]); });

$error = "";
$success = "";
if ($_POST["name"])
{
  $a = array();
  $a["name"] = $_POST["name"];
  $a["countryCode"] = $_POST["country"];
  foreach($ims as $imName=>$im)
  {
    $a[$imName] = $_POST[$imName];
    if ($im["transformInput"])
      $a[$imName] = $im["transformInput"]( $a[$imName] );
  }
  SQLLib::UpdateRow("users",$a,"id=".(int)$_SESSION["userID"]);
  
  header("Location: ".SITE_URL."profile/#success");
  exit();
}
else if ($_POST["eggdropUser"])
{
  function connectEggdrop()
  {
    $irc = new EggdropRPC();
    $response = $irc->connect(EGGDROP_RPC_HOST,EGGDROP_RPC_PORT);
    if ($response === false)
      return "Unable to connect to Tard!";
    if ($response[0] === "ERROR")
      return "Unable to connect to Tard: ".$response[1];
    
    if (!$irc->auth( EGGDROP_RPC_AUTHKEY ))
      return "Unable to authenticate with Tard!";
    
    if (!$irc->checkpass( $_POST["eggdropUser"], $_POST["eggdropPass"] ))
      return "Incorrect login info!";
    
    global $currentUser;

    $a = array();
    $currentUser->eggdropUser = $a["eggdropUser"] = $_POST["eggdropUser"];
    SQLLib::UpdateRow("users",$a,"id=".(int)$_SESSION["userID"]);
  }
  if (!($error = connectEggdrop()))
  {
    header("Location: ".SITE_URL."profile/#connect-success");
    exit();
  }
}

include_once("header.inc.php");

if ($error)
  printf("<div class='error'>%s</div>\n",_html($error));

echo "<h2>Your profile</h2>";

echo "<form method='post'>";
echo "  <label for='name'>Your name on the chat: <span class='required'>Required</span></label>";
echo "  <input type='text' name='name' id='name' value='"._html($currentUser->name)."' required='yes'/>";
echo "  <label for='country'>Your beloved homeland:</label>";
echo "  <select name='country' id='country'>";
echo "  <option value=''>... I'm not tellin >:(</option>";
foreach($countries as $cc => $c)
  echo "  <option value='".$cc."'".($cc==$currentUser->countryCode?" selected='selected'":"").">"._html($c["english"])."</option>";
echo "  </select>";

echo "  <p><b>Note:</b> The links below will only be visible for people who are logged in (except for your imgur profile link). <i>None of these fields are required.</i></p>";

foreach($ims as $imName=>$im)
{
  echo "  <label for='"._html($imName)."'>"._html($im["name"]).":";
  if ($im["note"])
    echo " <span class='note'>"._html($im["note"])."</span>";
  echo "</label>";
  
  $attr = array();
  $attr["type"] = $im["formType"] ?: "text";
  $attr["name"] = $imName;
  $attr["id"] = $imName;
  $attr["value"] = $currentUser->$imName;
  if ($im["maxlength"])
    $attr["maxlength"] = (int)$im["maxlength"];
  
  array_walk($attr,function(&$v,$k){ $v = sprintf("%s='%s'",$k,_html($v)); } );
  echo "  <input ".implode(" ",array_values($attr))."/>";
}

echo "  <input type='submit' value='Send!'>";
echo "</form>";

echo "<h2>Your voucher code</h2>";
echo "<pre id='voucher'>"._html($currentUser->voucherCode)."</pre>";
echo "<p><b>Note:</b> this code is one-time-use-only. If whoever you gave it to used it, you will get a new one.</p>";
echo "<p><b>More important note:</b> Whoever you give this code to will see not only your personal information you provided above, but everyone else's from the channel. Don't just give it to someone who joined 5 minutes ago.</p>";

echo "<h2>Connect to Tard</h2>";
if ($currentUser->eggdropUser)
{
  echo "<p>Connected as <b>"._html($currentUser->eggdropUser)."</b></p>";
}
else
{
  echo "<p>If you've been on the channel before, you probably met Tard. If you have an account to it, you can connect to it here.</p>";
  echo "<p>Right now the functionality you can reach with it is pretty limited, but we'll expand on it as we go!</p>";
  echo "<form method='post'>";
  echo "  <label for='eggdropUser'>Your username for Tard: <span class='required'>Required</span></label>";
  echo "  <input type='text' name='eggdropUser' id='eggdropUser' required='yes'/>";
  echo "  <label for='eggdropPass'>Your password for Tard: <span class='required'>Required</span></label>";
  echo "  <input type='password' name='eggdropPass' id='eggdropPass' required='yes'/>";
  echo "  <input type='submit' value='Connect!'>";
  echo "</form>";
}

include_once("footer.inc.php");
?>
