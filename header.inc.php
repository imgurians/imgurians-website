<!DOCTYPE html>
<html>
<head>
  <title>#imgurians<?=($TITLE?" - ".$TITLE:"")?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
  <link rel="stylesheet" type="text/css" href="<?=SITE_URL?>style.css?<?=filemtime("style.css")?>" media="screen" />
  <link rel="shortcut icon" href="<?=SITE_URL?>favicon.ico" type="image/x-icon"/>
  <link rel="alternate" type="application/rss+xml" title="RSS" href="<?=SITE_URL?>rss/"/>
  <link rel="alternate" type="application/rss+xml" title="Quotes RSS" href="<?=SITE_URL?>rss/quotes/"/>
  <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/prototype/1.7.2.0/prototype.js"></script>
  <meta name="description" content="#imgurians - the home of the best imgur chat"/>
  <meta name="keywords" content="imgur,imgurians,irc,chat,awesome"/>
</head>
<body>

<div id="container">
  <header>
    <h1>#imgurians</h1>
    <a id='chatlink' href="<?=SITE_URL?>chat/">Chat now!</a>
    <nav>
      <ul>
        <li><a href='<?=SITE_URL?>'>News</a></li>
        <li><a href='<?=SITE_URL?>faq/'>FAQ</a></li>
        <li><a href='<?=SITE_URL?>people/'>People</a></li>
        <li><a href='<?=SITE_URL?>gallery/'>Gallery</a></li>
        <li><a href='<?=SITE_URL?>events/'>Events</a></li>
        <li><a href='<?=SITE_URL?>topics/'>Topics</a></li>
        <li><a href='<?=SITE_URL?>quotes/'>Quotes</a></li>
<? if ($_SESSION["userID"]) { ?>
        <li><a href='<?=SITE_URL?>profile/'>Your info (<?=_html($currentUser->name)?>)</a></li>
        <li><a href='<?=SITE_URL?>logout/'>Log out</a></li>
<? } else { ?>
        <li><a href='<?=SITE_URL?>login/'>Log in</a></li>
<? } ?>
      </ul>
    </nav>
  </header>
  
  <div id="content">

